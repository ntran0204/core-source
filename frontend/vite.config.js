import { defineConfig, loadEnv } from "vite";
import path, { dirname } from "path";
import { fileURLToPath } from "url";
import react from "@vitejs/plugin-react";
import { flowPlugin, esbuildFlowPlugin } from "@bunchtogether/vite-plugin-flow";

export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, path.resolve(process.cwd(), "../"), "");

  if (env.npm_lifecycle_event === "build" && !env.CI && !env.SHOPIFY_API_KEY) {
    console.warn(
      "\nBuilding the frontend app without an API key. The frontend build will not run without an API key. Set the SHOPIFY_API_KEY environment variable when running the build command.\n",
    );
  }
  1;

  const proxyOptions = {
    target: `http://127.0.0.1:${env.BACKEND_PORT}`,
    changeOrigin: false,
    secure: true,
    ws: false,
  };

  const host = env.APP_HOST ? env.APP_HOST.replace(/https?:\/\//, "") : "localhost";

  let hmrConfig;
  if (host === "localhost") {
    hmrConfig = {
      protocol: "ws",
      host: "localhost",
      port: 64999,
      clientPort: 64999,
    };
  } else {
    hmrConfig = {
      protocol: "wss",
      host: host,
      port: env.FRONTEND_PORT,
      clientPort: 443,
    };
  }

  let subHost = env.SHOPIFY_SHOP.split(".")[0];

  let base64Host = Buffer.from("admin.shopify.com/store/" + subHost, "utf8").toString("base64");
  return {
    root: dirname(fileURLToPath(import.meta.url)),
    optimizeDeps: {
      esbuildOptions: {
        define: {
          global: "window",
        },
        plugins: [esbuildFlowPlugin()],
      },
    },
    plugins: [flowPlugin(), react()],
    css: {
      modules: {
        localsConvention: "camelCase",
      },
    },
    //defines process env here
    define: {
      "process.env.SHOPIFY_API_KEY": JSON.stringify(env.SHOPIFY_API_KEY),
    },
    resolve: {
      preserveSymlinks: true,
    },
    build: {
      target: "esnext",
      rollupOptions: {
        input: {
          main: path.resolve(process.cwd(), "index.html"),
          install: path.resolve(process.cwd(), "install.html"),
        },
      },
    },
    server: {
      host: "localhost",
      port: env.FRONTEND_PORT,
      hmr: hmrConfig,
      open: `http://localhost:${env.FRONTEND_PORT}?shop=${env.SHOPIFY_SHOP}&host=${base64Host}`,
      proxy: {
        "^/(\\?.*)?$": proxyOptions,
        "^/api(/|(\\?.*)?$)": proxyOptions,
      },
    },
  };
});
