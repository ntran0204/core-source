import axios from "axios";

const { API_VER } = process.env;

const api = axios.create();

const resquestUrl = ({ shop, endpoint }) => {
  return `https://${shop}/admin/api/${API_VER}/${endpoint}`;
};

const generateOptions = ({ accessToken, options = {} }) => ({
  headers: {
    "Content-Type": "application/json",
    "X-Shopify-Access-Token": accessToken,
    ...options,
  },
});

api.interceptors.response.use(function (response) {
  return response.data;
});

const ShopifyApi = {
  get({ shop, accessToken, endpoint }) {
    try {
      return api.get(resquestUrl({ shop, endpoint }), generateOptions({ accessToken }));
    } catch (error) {
      throw error;
    }
  },

  async post({ shop, accessToken, endpoint, data, options }) {
    try {
      const response = await api.post(resquestUrl({ shop, endpoint }), data, generateOptions({ accessToken, options }));
      return response;
    } catch (error) {
      console.log("error :>> ", error);
      throw error;
    }
  },

  async put({ shop, accessToken, endpoint, data }) {
    try {
      return await api.put(resquestUrl({ shop, endpoint }), data, generateOptions({ accessToken }));
    } catch (error) {
      throw error;
    }
  },
};

export default ShopifyApi;
