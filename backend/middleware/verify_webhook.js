import crypto from "crypto";

const verifyWebhook = (req, res, next) => {
  let topic = req.headers["x-shopify-topic"];
  let hmac = req.headers["x-shopify-hmac-sha256"];
  let shop = req.headers["x-shopify-shop-domain"];
  let version = req.headers["x-shopify-api-version"];

  let digest = crypto
    .createHmac("sha256", process.env.SHOPIFY_API_SECRET)
    .update(req.rawBody)
    .digest("base64");

  if (hmac === digest) {
    req.state = { topic, shop };
    next();
    return;
  }
  res.status(401).send("Invalid HMAC");
};

export default verifyWebhook;
