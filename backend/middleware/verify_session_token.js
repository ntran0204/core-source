import JWT from "jsonwebtoken";
import redirectToShopifyInstall from "../helpers/redirect_to_shopify_install";
import ResponseHandler from "../helpers/response_handler";
import StoresService from "../services/stores.service";

export default async function verifySessionToken(req, res, next) {
  let { authorization } = req.headers;

  if (!authorization) {
    ResponseHandler.error(res, error, 401);
    return;
  }

  let token = authorization.split(" ")[1];

  let [error, payload] = await new Promise((resolve, reject) => {
    JWT.verify(
      token,
      process.env.SHOPIFY_API_SECRET,
      { ignoreExpiration: true },
      (error, payload) => {
        if (error) {
          resolve([error, null]);
        } else {
          resolve([null, payload]);
        }
      },
    );
  });

  if (error) {
    ResponseHandler.error(res, error, 401);
    return;
  }

  let shop = new URL(payload.dest).host;

  let accessToken = await StoresService.getAccessToken(shop);

  if (!accessToken) {
    return redirectToShopifyInstall({ res, shop });
  }

  req.state = {
    shop,
    accessToken,
  };

  next();
}
