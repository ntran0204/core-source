import StoresService from "../services/stores.service";

export default async function checkShopInstalled(shop) {
  try {
    const storeSettings = await StoresService.checkInstalled(shop);
    return !!storeSettings;
  } catch (error) {
    throw error;
  }
}
