const { SHOPIFY_API_KEY } = process.env;
export default function redirectToAdminApp({ res, shop }) {
  try {
    return res.redirect(`https://admin.shopify.com/store/${shop.split(".")[0]}/apps/${SHOPIFY_API_KEY}`);
  } catch (error) {
    throw error;
  }
}
