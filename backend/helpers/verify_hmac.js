import crypto from "crypto";

export default function verifyHmac(req) {
  let { hmac } = req.query;

  let searchParams = new URLSearchParams(req.query);
  searchParams.delete("hmac");

  const digest = crypto
    .createHmac("sha256", process.env.SHOPIFY_API_SECRET)
    .update(searchParams.toString())
    .digest("hex");

  if (digest !== hmac) {
    return false;
  }
  return true;
}
