const ResponseHandler = {
  success: (res, data) => {
    res.status(200).json({
      success: true,
      data,
    });
  },
  error: (res, error) => {
    res.status(error?.statusCode || 500).json({
      success: false,
      error: { message: error.message },
    });
  },
};

export default ResponseHandler;
