import StoresService from "../services/stores.service";
import nonceCreate from "nonce";

const { SHOPIFY_APP_HOST, SHOPIFY_API_KEY, SCOPE } = process.env;

export default async function redirectToShopifyInstall({ res, shop }) {
  try {
    // send a nonce variable to Shopify app install page to install step check this value
    let nonce = nonceCreate()();
    // After app installed, shopify will send a request to callBackLink endpoint
    let callBackLink = `${SHOPIFY_APP_HOST}/api/shopify/auth/callback`;

    // Save to database for '/auth/callback' endpoint check nonce value
    await StoresService.saveNonce({ shop, nonce });

    return res.redirect(
      `https://${shop}/admin/oauth/authorize?client_id=${SHOPIFY_API_KEY}&scope=${SCOPE}&redirect_uri=${callBackLink}&state=${nonce}`,
    );
  } catch (error) {
    console.log("error:", error);
    throw { message: error.message, status: "error" };
  }
}
