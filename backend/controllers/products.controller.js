import productCreator from "../helpers/product-creator";
import ResponseHandler from "../helpers/response_handler";
import ShopifyService from "../services/shopify/shopify.service";

const ProductsController = {
  async count(req, res) {
    try {
      const { shop, accessToken } = req.state;

      const data = await ShopifyService.rest.products.count({ shop, accessToken });

      return ResponseHandler.success(res, data);
    } catch (error) {
      return ResponseHandler.error(res, error);
    }
  },

  async create(req, res) {
    try {
      const { shop, accessToken } = req.state;
      const productData = await productCreator();

      const createdProduct = await ShopifyService.rest.products.create({ shop, accessToken, data: productData });
      return ResponseHandler.success(res, createdProduct);
    } catch (error) {
      return ResponseHandler.error(res, error);
    }
  },
};

export default ProductsController;
