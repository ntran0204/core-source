import StoresModel from "../models/stores.model";

export default async function ShopifyWebhooksController(req, res) {
  let { topic, shop } = req.state;

  res.status(200).send();

  switch (topic) {
    case "app/uninstalled":
      let record = await StoresModel.update(
        { status: "UNINSTALL" },
        {
          where: {
            shop,
          },
        },
      );

      break;

    default:
      break;
  }
}
