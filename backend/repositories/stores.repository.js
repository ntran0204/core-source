import StoresModel from "../models/stores.model";

const StoresRepository = {
  async create(data) {
    return await StoresModel.create(data);
  },

  async upsert(values) {
    try {
      const { shop } = values;
      let recordFound = await StoresModel.findOne({ where: { shop } });
      let newRecord;

      if (recordFound) {
        newRecord = await recordFound.update(values);
      } else {
        newRecord = await StoresModel.create(values);
      }

      return newRecord?.toJSON();
    } catch (error) {
      throw error;
    }
  },

  async findOne(...args) {
    try {
      let record = await StoresModel.findOne(...args);

      return record;
    } catch (error) {
      throw error;
    }
  },

  async findByShop(shop, attributes) {
    try {
      return await StoresModel.findOne({ where: { shop }, attributes });
    } catch (error) {
      throw error;
    }
  },
};

export default StoresRepository;
