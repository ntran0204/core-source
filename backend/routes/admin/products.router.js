import express from "express";
import ProductsController from "../../controllers/products.controller";

const ProductsRouter = express.Router();

ProductsRouter.get("/count", ProductsController.count);
ProductsRouter.get("/create", ProductsController.create);

export default ProductsRouter;
