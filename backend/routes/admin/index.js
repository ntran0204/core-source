import express from "express";
import ProductsRouter from "./products.router";

const AdminRouter = express.Router();

AdminRouter.use("/products", ProductsRouter);

export default AdminRouter;
