import express from "express";
import verifyHmac from "../../helpers/verify_hmac";
import StoresRepository from "../../repositories/stores.repository";
import ShopifyService from "../../services/shopify/shopify.service";
import StoresService from "../../services/stores.service";

const ShopifyAuthRoutes = express.Router();

const { SHOPIFY_API_KEY, SHOPIFY_API_SECRET } = process.env;

ShopifyAuthRoutes.get("/callback", async (req, res) => {
  try {
    let { code, shop, state, host } = req.query;

    if (!verifyHmac(req)) {
      res.status(401).json({ error: "Invalid HMAC" });
      return;
    }

    if (!(await StoresService.checkNonce({ shop, state }))) {
      res.status(401).json({ error: "Invalid nonce" });
      return;
    }

    let accessTokenRes = await ShopifyService.rest.auth.getAccessToken({
      shop,
      data: {
        client_id: SHOPIFY_API_KEY,
        client_secret: SHOPIFY_API_SECRET,
        code,
      },
    });

    let shopRes = await ShopifyService.rest.shop.get({
      shop,
      accessToken: accessTokenRes.access_token,
    });

    await StoresRepository.upsert({
      shop,
      appPlan: "free",
      storePlan: shopRes.shop.plan_name,
      accessToken: accessTokenRes.access_token,
      status: "installed",
      scope: accessTokenRes.scope,
      email: shopRes.shop.email,
      phone: shopRes.shop.phone,
      shopOwner: shopRes.shop.shop_owner,
    });

    ShopifyService.rest.webhooks.init({ shop, accessToken: accessTokenRes.access_token });
    const decodedBuffer = Buffer.from(host, "base64");
    const decodedString = decodeURIComponent(decodedBuffer.toString());

    //Redirect to Shopify App after installation is done
    res.redirect(`https://${decodedString}/apps/${SHOPIFY_API_KEY}`);
  } catch (error) {
    console.log("error:", error);
    res.status(500).send(error.message);
  }
});

export default ShopifyAuthRoutes;
