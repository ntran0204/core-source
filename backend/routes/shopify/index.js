import express from "express";
import verifyWebhook from "../../middleware/verify_webhook";
import ShopifyAuthRoutes from "./auth";
import ShopifyWebhooksRoutes from "./webhooks";

const ShopifyRouter = express.Router();

ShopifyRouter.use("/auth", ShopifyAuthRoutes);

ShopifyRouter.use("/webhooks", verifyWebhook, ShopifyWebhooksRoutes);

export default ShopifyRouter;
