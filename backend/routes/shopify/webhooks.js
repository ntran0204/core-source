import express from "express";
import ShopifyWebhooksController from "../../controllers/shopify_webhooks.controller";

const ShopifyWebhooksRoutes = express.Router();

ShopifyWebhooksRoutes.post("/", ShopifyWebhooksController);

export default ShopifyWebhooksRoutes;
