import StoresRepository from "../repositories/stores.repository";

const StoresService = {
  async findOne(...args) {
    try {
      return await StoresRepository.findOne(...args);
    } catch (error) {
      throw error;
    }
  },
  async saveNonce({ shop, nonce }) {
    try {
      let result = await StoresRepository.upsert({ shop, nonce });
      return result;
    } catch (error) {
      throw error;
    }
  },

  async checkNonce({ shop, state }) {
    try {
      let record = await StoresRepository.findOne({ where: { shop } });

      if (!record) {
        return false;
      }

      if (record.nonce !== state) {
        return false;
      }

      return true;
    } catch (error) {
      throw error;
    }
  },

  async checkInstalled(shop) {
    let record = await StoresRepository.findByShop(shop, ["status"]);

    if (!record) {
      return false;
    }

    if (!["installed", "running"].includes(record.status.toLowerCase())) {
      return false;
    }

    return true;
  },

  async getAccessToken(shop) {
    try {
      let record = await StoresRepository.findOne({ where: { shop } });

      if (!record) {
        return null;
      }

      return record.accessToken;
    } catch (error) {
      throw error;
    }
  },
};

export default StoresService;
