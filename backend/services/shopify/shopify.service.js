import restWebhooks from "./rest/webhooks";
import restAuth from "./rest/auth";
import restShop from "./rest/shop";
import restThemes from "./rest/themes";
import restMetafield from "./rest/metafield";
import ShopifyApi from "../../apis/shopify";
import restProducts from "./rest/products";

const ShopifyService = {
  rest: {
    auth: restAuth,
    shop: restShop,
    webhooks: restWebhooks,
    themes: restThemes,
    metafield: restMetafield,
    products: restProducts,
  },
};

export default ShopifyService;
