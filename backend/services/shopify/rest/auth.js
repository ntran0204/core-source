import axios from "axios";

const restAuth = {
  async getAccessToken({ shop, data }) {
    try {
      let response = await axios({
        url: `https://${shop}/admin/oauth/access_token`,
        method: "post",
        headers: { "Content-Type": "application/json" },
        data,
      });

      return response.data;
    } catch (error) {
      console.log("restAuth getAccessToken error :>> ", error);
    }
  },
};

export default restAuth;
