import ShopifyApi from "../../../apis/shopify";

const restProducts = {
  async count({ shop, accessToken }) {
    try {
      return await ShopifyApi.get({ shop, accessToken, endpoint: "/products/count.json" });
    } catch (error) {
      throw error;
    }
  },

  async create({ shop, accessToken, data }) {
    try {
      return await ShopifyApi.post({ shop, accessToken, endpoint: "/products.json", data });
    } catch (error) {
      throw error;
    }
  },
};

export default restProducts;
