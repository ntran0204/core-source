import ShopifyApi from "../../../apis/shopify";

const restMetafield = {
  async post({ shop, accessToken, endpoint, data }) {
    return await ShopifyApi.post({ shop, accessToken, endpoint, data });
  },
};
export default restMetafield;
