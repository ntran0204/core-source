import ShopifyApi from "../../../apis/shopify";

const { SHOPIFY_APP_HOST } = process.env;

const restWebhooks = {
  async init({ shop, accessToken }) {
    try {
      let topics = ["app/uninstalled"];

      for (let index = 0; index < topics.length; index++) {
        const topic = topics[index];

        let response = await this.register({ shop, accessToken, topic });
      }
    } catch (error) {
      console.log("ShopifyService init webhook error:", error);
      throw error;
    }
  },

  async register({ shop, accessToken, topic, fields = [] }) {
    try {
      let response = await ShopifyApi.post({
        shop,
        endpoint: "webhooks.json",
        data: {
          webhook: {
            address: `${SHOPIFY_APP_HOST}/api/shopify/webhooks`,
            topic,
            format: "json",
            fields,
          },
        },
        accessToken,
      });
      // if (error) {
      //   console.log("error:", error);
      //   throw error;
      // }

      return response;
    } catch (error) {
      console.log("ShopifyService register webhook error:", error);
      throw error;
    }
  },
};

export default restWebhooks;
