import ShopifyApi from "../../../apis/shopify";

const restShop = {
  async get({ shop, accessToken, fields }) {
    try {
      let response = await ShopifyApi.get({
        accessToken,
        shop,
        endpoint: `/shop.json${fields ? `?fields=${fields.join(",")}` : ""}`,
      });

      return response;
    } catch (error) {
      console.log("restShop get error :>> ", error.message);
    }
  },
};

export default restShop;
