import ShopifyApi from "../../../apis/shopify";

const restThemes = {
  async get({ shop, accessToken }) {
    try {
      let response = await ShopifyApi.get({ shop, accessToken, endpoint: "themes.json" });
      return response;
    } catch (error) {
      throw error;
    }
  },

  async checkEnable({ shop, accessToken, themeId }) {
    let enableSettings = process.env.SHOPIFY_EXTENSION_BLOCKS.split(",");

    let settingsData = await getSettingsData({ shop, themeId, accessToken });

    let settingsEnabled = Object.keys(settingsData.current.blocks).reduce((accu, blockId) => {
      let block = settingsData.current.blocks[blockId];

      if (block.type.includes(process.env.SHOPIFY_EXTENSION_ID)) {
        let settingFound = enableSettings.find((setting) => {
          if (block.type.includes(setting) && block.disabled == false) {
            return true;
          }
          return false;
        });

        settingFound && accu.push(settingFound);
      }
      return accu;
    }, []);

    return {
      settingsEnabled,
    };
  },

  async enableAppSettings({ shop, accessToken, themeId, settings }) {
    try {
      let enableSettings = settings instanceof Array ? settings : [settings];
      let resAsset = await ShopifyApi.get({
        shop,
        accessToken,
        endpoint: `themes/${themeId}/assets.json?asset[key]=config/settings_data.json`,
      });
      let settingsData = JSON.parse(resAsset.asset.value);
      settingsData.current.blocks = settingsData.current.blocks || {};

      for (let index = 0; index < enableSettings.length; index++) {
        const settingName = enableSettings[index];

        if (checkSettingEnabled(settingName, settingsData)) {
          continue;
        }

        let blockId = `${settingName}-${process.env.SHOPIFY_EXTENSION_ID}`;

        settingsData.current.blocks[blockId] = {
          type: `shopify://apps/wishlist/blocks/${settingName}/${process.env.SHOPIFY_EXTENSION_ID}`,
          disabled: false,
        };
      }

      let data = {
        asset: {
          key: "config/settings_data.json",
          value: JSON.stringify(settingsData),
        },
      };

      await ShopifyApi.put({
        shop,
        accessToken,
        endpoint: `themes/${themeId}/assets.json`,
        data,
      });

      return;
    } catch (error) {
      console.log("restTheme enableAppSettings error :>>", error.message);
      throw getError(error);
    }
  },
};

function checkSettingEnabled(settingName, settingsData) {
  let blockId = Object.keys(settingsData.current.blocks).find((blockId) => {
    let block = settingsData.current.blocks[blockId];
    return (
      block.type.includes(process.env.SHOPIFY_EXTENSION_ID) && block.type.includes(settingName)
    );
  });

  if (blockId) {
    settingsData.current.blocks[blockId].disabled = false;
    return true;
  }

  return false;
}
// get content settings_data.json of theme
//return settings data object
async function getSettingsData({ shop, accessToken, themeId }) {
  try {
    let response = await ShopifyApi.get({
      shop,
      accessToken,
      endpoint: `themes/${themeId}/assets.json?asset[key]=config/settings_data.json`,
    });

    return JSON.parse(response.asset.value);
  } catch (error) {
    throw error;
  }
}

function getError(error) {
  if ("response" in error) {
    switch (error.response.status) {
      case 404:
        return { message: "Theme Not Found", status: 404 };
        break;
      default:
        return { message: error.message, status: error.response.status };
        break;
    }
  } else {
    return { message: error.message, status: 500 };
  }
}

export default restThemes;
