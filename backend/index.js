import path, { join } from "path";
import "./helpers/dotenv_config";
import { readFileSync } from "fs";
import express from "express";
import cors from "cors";
import serveStatic from "serve-static";
import ensureInstalledOnShop from "./middleware/ensure_installed_on_shop";
import verifySessionToken from "./middleware/verify_session_token";
import ShopifyRouter from "./routes/shopify";
import AdminRouter from "./routes/admin";

const PORT = parseInt(process.env.BACKEND_PORT || process.env.PORT || "3000", 10);

const STATIC_PATH = (process.env.STATIC_PATH =
  process.env.NODE_ENV === "production"
    ? path.resolve(process.cwd(), "../frontend/dist")
    : path.resolve(process.cwd(), "../frontend"));

const app = express();

app.use(cors());
app.use(serveStatic(STATIC_PATH, { index: false }));
app.use(express.urlencoded({ extended: true }));

// parse application/json
app.use(
  express.json({
    verify: function (req, res, buf, encoding) {
      if (req.path.startsWith("/api/shopify/webhooks")) {
        req.rawBody = buf.toString();
      }
    },
  }),
);

// This router handle the requests from the Shopify (auth, webhook, etc.)
app.use("/api/shopify", ShopifyRouter);

//This router handle the requests from embed app
app.use("/api/admin", verifySessionToken, AdminRouter);

app.use("/*", ensureInstalledOnShop, async (_req, res, _next) => {
  return res
    .status(200)
    .set("Content-Type", "text/html")
    .send(readFileSync(join(STATIC_PATH, "index.html")));
});

app.listen(5001);
