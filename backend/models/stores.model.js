import { DataTypes } from "sequelize";
import PostgresSequelize from "../connector/postgres";

const StoresModel = PostgresSequelize.define("stores", {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  shop: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: "compositeIndex",
  },
  appPlan: {
    type: DataTypes.STRING, //FREE, BASIC, PRO, PLUS
    defaultValue: "FREE",
  },
  storePlan: {
    type: DataTypes.STRING,
  },
  accessToken: {
    type: DataTypes.STRING,
  },
  status: {
    type: DataTypes.STRING, // INSTALLED, RUNNING, UNINSTALLED, LOCKED, INSTALLING
    defaultValue: "INSTALLING",
  },
  email: {
    type: DataTypes.STRING,
  },
  phone: {
    type: DataTypes.STRING,
  },
  shopOwner: {
    type: DataTypes.STRING,
  },
  scope: {
    type: DataTypes.STRING,
  },
  acceptedAt: {
    type: DataTypes.DATE,
  },
  nonce: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

StoresModel.sync({ alter: process.env.NODE_ENV == "development" ? true : false }).then(() => {});

export default StoresModel;
